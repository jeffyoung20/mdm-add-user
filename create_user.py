import json
import datetime
import requests
import re
import sys
import argparse


tenant_id = ""
admin_user = ""
admin_password = ""
configFilePath = ""
clientName = ""
forceUpdate = False

configFile = {}
cloudFormationParamFile = {}
replacementValues = {}

def main(): 
    print "***** START *****"
    print "" # skip a line

    #Read Command Line
    print "Command Line Params: "
    parser = argparse.ArgumentParser()
    parser.add_argument("configFilePath", help="path to config file")
    parser.add_argument("paramFileName", help="Path to CloudFormation Param File")
    parser.add_argument("admin_user", help="Reltio admin use id")
    parser.add_argument("admin_password", help="Reltio admin password")
    parser.add_argument("clientName", help="Client Name")
    parser.add_argument("--forceUpdate", help="Force Update to Reltio User", action="store_true")
    args = parser.parse_args()

    print "Config File Path: " + args.configFilePath
    global configFilePath
    configFilePath = args.configFilePath
    print "CloudFormation Param File Path: " + args.paramFileName
    global paramFileName
    paramFileName = args.paramFileName
    print "Admin User Name: " + args.admin_user
    global admin_user
    admin_user = args.admin_user    
    print "Admin User Password: " + args.admin_password
    global admin_password
    admin_password = args.admin_password    
    print "Client Name: " + args.clientName
    global clientName
    clientName = args.clientName
    print "Force Update: " + str(args.forceUpdate)
    global forceUpdate
    forceUpdate = args.forceUpdate
    print "" #skip a space

    #Read Config File
    print "Reading Config File ..."
    global configFile
    configFile = readConfigFile(configFilePath)
 
    #Read CloudFormation Param File
    print "Reading CloudFormation Param File ....."
    readCFParamFile(paramFileName)

    #Read User Roles Param File
    print "Reading User Roles File ..."
    userRolesFile = configFile['userRolesFilePath']
    userRoles = readUserRolesFile(userRolesFile)
    readUserRolesFileReplVars(configFile['ReplacementParams'] )

    #Get Access Token
    print "Getting Access Token ..."
    tenant_id = getAdminTenant(configFile)    
    print "Teanant Id: " + tenant_id
    access_token = authenticate(admin_user, admin_password)
    print "Access Token: " + access_token

    #Get List of Current users
    print ""
    print "Getting User List ..."
    userListReltio = getUserList(access_token)

    #***** Add New Users *****
    updateCloudFormParamFile = False
    mappings = configFile['cloudFormationParamFileUserMappings']
    for item in mappings:
        print ""    #skip a line
        bProposedUpdate = False
        userType = item['userType']
        userNameFieldName = item['userNameField']
        passwordFieldName = item['passwordField']
        userName = getCFParamFileValue(userNameFieldName)
        password = getCFParamFileValue(passwordFieldName)

        roles = userRoles[userType]['roles']
        roles = replParamsInArray(roles)
        tenants = userRoles[userType]['tenants']
        tenants = replParamsInArray(tenants)

        userReltio = getUser(userListReltio, userName)
        if userReltio is None:
            if(len(userName.strip()) == 0):
                userName = applyReplacmentValues(item["defaultUsername"])
                setCFParamFileValue(userNameFieldName, userName)
                bProposedUpdate = True
                print "Username not found.  Created user name: " + userName
            if(len(password.strip()) == 0):
                password = generate_password(10)
                setCFParamFileValue(passwordFieldName, password) 
                bProposedUpdate = True
                print "Password not found.  Created password: " + password

            print "Adding User " + userName + " ... "
            bSuccess = create_user(access_token, userName, password, roles, tenants)  
            if bSuccess and bProposedUpdate:
                updateCloudFormParamFile = True    
        elif forceUpdate == True: 
            #***** UPDATE USER *****
            print "User " + userName + " already exists in Reltio"
            print "Updating user ..."
            newRoles = list(set(roles) - set (userReltio["roles"]))
            print "Roles To Add: " + str(newRoles)
            reltioNewRoles = list(set(roles) | set (newRoles))

            newTenants = list(set(tenants) - set (userReltio["tenants"]))
            print "Tenants To Add: " + str(newTenants)
            reltioNewTenants = list(set(tenants) | set (newTenants))

            if len(newRoles) > 0 or len(newTenants) > 0:
                userReltio["tenants"] = reltioNewTenants
                userReltio["roles"] = reltioNewRoles
                bSuccess = updateUser(access_token, userName, password, userReltio)  
                if bSuccess and bProposedUpdate:
                    updateCloudFormParamFile = True   
            else:
                print "No differences found.  Update is not necessary"
        else:
            print "User " + userName + " already exists in Reltio"

    #Check if we need to update the CloudFormation params file
    if updateCloudFormParamFile:
        print ""
        print "Update CF Param File"
        with open(paramFileName, 'w') as outfile:
            json.dump(cloudFormationParamFile, outfile)
    else:
        print ""
        print "NO updates to Config File"

    print "" #skip a line
    print "***** END *****"


def readConfigFile(fileName):
    with open(fileName) as json_data:
        data = json.load(json_data)
        return data   

def readUserRolesFile(fileName):
    with open(fileName) as json_data:
        data = json.load(json_data)
        return data

def readCFParamFile(fileName):
    with open(fileName) as json_data:
        data = json.load(json_data)
        global cloudFormationParamFile
        cloudFormationParamFile = data

def setCFParamFileValue(key, value):
    for index, elem in enumerate(cloudFormationParamFile):
        if key == elem["ParameterKey"]:
            elem["ParameterValue"] = value

def getCFParamFileValue(key):
    retVal = ""
    for index, elem in enumerate(cloudFormationParamFile):
        if key == elem["ParameterKey"]:
            retVal = elem["ParameterValue"]
    return retVal

def readUserRolesFileReplVars(configFileReplParams):
    for item in configFileReplParams:
        replVariable = item['replacementVar']
        replValue = getCFParamFileValue(item['paramFileField'])
        if replValue.startswith("http"):    #TODO:  do something different here
            index = replValue.rfind("/")
            replValue = replValue[index+1:]
        replacementValues[replVariable] = replValue

def getReltioAuthUrl():
    return configFile["reltioAuthUrl"]

def getAdminTenant(configFile):
    tenants = configFile["ReplacementParams"]
    tenantIdField =""
    for tenant in tenants:
        if tenant["replacementVar"].lower() == "tenant_id":
            tenantIdField = tenant["paramFileField"]
    if len(tenantIdField) == 0 :
        raise ValueError("String tenant_id not found in replacment params in the config file")
    adminTenantId = getCFParamFileValue(tenantIdField)

    #Verify tenant id is not complete url
    if adminTenantId.startswith("http"):    
        index = adminTenantId.rfind("/")
        adminTenantId = adminTenantId[index+1:]

    return adminTenantId

def replParamsInArray(arr):
    for index, item in enumerate(arr):
        item = applyReplacmentValues(item)
        # for replKey, replValue in replacementValues.iteritems():
        #     item = re.sub("\${" + replKey + "}",replValue, item)
        arr[index] = item
    return arr

def applyReplacmentValues(originalValue):
    value = originalValue
    value = re.sub("\${clientname}", clientName, value)
    for replKey, replValue in replacementValues.iteritems():
        value = re.sub("\${" + replKey + "}",replValue, value)    
    return value

def generate_password(length):
    if not isinstance(length, int) or length < 8:
        raise ValueError("temp password must have positive length")

    chars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    from os import urandom
    return "".join(chars[ord(c) % len(chars)] for c in urandom(length))

def getUser(reltioUsersArr, username):
    #userExisits = False
    reletioUser = None
    for user in reltioUsersArr:
        reltioUserName = user["username"]
        if username == reltioUserName:
            reletioUser = user
    return reletioUser


def authenticate(UserName, Password):
    auth_url ="https://auth.reltio.com/oauth//token"
    headers = {
        'authorization': "Basic cmVsdGlvX3VpOm1ha2l0YQ==",
        'cache-control': "no-cache"
        }	
    querystring = {"username":UserName,"password":Password,"grant_type":"password"}
    response = requests.request("GET", auth_url, headers=headers, params=querystring)
    json_data = json.loads(response.text)
    return json_data.get("access_token")

def updateUser(AccessToken, UserName, Password, jsonBody):
    headers = {
        'authorization': "Bearer %s" % (AccessToken),
        'content-type': "application/json",
        'cache-control': "no-cache"
    }

    payload = json.dumps(jsonBody)
    url = getReltioAuthUrl() + "//users//" + UserName
    response = requests.request("PUT", url, data=payload, headers=headers)
    result = False
    if response.status_code > 201 :
        j = json.loads(response.text)
        print "ERROR Code:  " + str(response.status_code)
        print "ERROR Msg:  " + j["errorMessage"]
        print(payload)    
    else:
        print "Reponse: " + str(response.status_code)
        print "SUCCESS"
        result = True
    return result


def create_user(AccessToken, UserName, Password, RolesArr, TenantsArr):
    email = UserName + "@us.imshealth.com"  #TODO:  Verify what i should do here
    payload = [
        {
        "customer":"IMS",
        "username": UserName,
        "email": email,
        "roles": RolesArr ,
        "tenants": TenantsArr,
        "enabled": True,
        "accountNonLocked": True,
        "password":Password,
        "accountNonExpired": True,
        "credentialsNonExpired": True,
        "locale": "en",
        "timezone": "UTC"
        }
    ]
    headers = {
        'authorization': "Bearer %s" % (AccessToken),
        'content-type': "application/json",
        'cache-control': "no-cache"
    }

    url = getReltioAuthUrl() + "//users"
    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
    result = False
    if response.status_code > 201 :
        j = json.loads(response.text)
        print "ERROR Code:  " + str(response.status_code)
        print "ERROR Msg:  " + j["errorMessage"]
        print(payload)    
    else:
        print "Reponse: " + str(response.status_code)
        print "SUCCESS"
        result = True
    return result

def getUserList(AccessToken):
    url = "https://auth.reltio.com/oauth//users"
    headers = {
        'authorization': "Bearer %s" % (AccessToken),
        'content-type': "application/json",
        'cache-control': "no-cache"
    }
    resp = requests.request("GET", url, headers=headers)
    print "Reponse: " + str(resp.status_code)

    usersArr = json.loads(resp.text)
    return usersArr

# Execute main function if the script is being used as standalone
if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print("ERROR:")
        print(e)
